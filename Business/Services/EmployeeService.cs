﻿using AutoMapper;
using DTOModels;
using FirstWebApiApp.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirstWebApiApp.Utils;
using DTOModels;
using FirstWebApiApp.Models;

namespace FirstWebApiApp.Business
{
    public class EmployeeService : IEmployeeService
    {
        IMapper _mapper;
        IEmployeeRepository _employeeRepository;

        public EmployeeService(IMapper mapper, IEmployeeRepository employeeRepository)
        {
            _mapper = mapper;
            _employeeRepository = employeeRepository;
        }

        public async Task<EmployeeDTO> CreateEmployee(EmployeeDTO employee)
        {
            Employee newEmployee = _mapper.Map<Employee>(employee);
            Employee res = await _employeeRepository.CreateEmployee(newEmployee);
            EmployeeDTO resEmployee =  _mapper.Map<EmployeeDTO>(res);
            return resEmployee;
        }

        public async Task<List<EmployeeDTO>> GetAllEmployees()
        {           
            return _mapper.Map<List<EmployeeDTO>>(await _employeeRepository.GetAllEmployees());
        }

        public async Task<EmployeeDTO> GetEmployeeById(string id)
        {
            return _mapper.Map<EmployeeDTO>(await _employeeRepository.GetEmployeeById(int.Parse(EncryptionUtility.Decrypt(id))));
        }

        public async Task<object> GetEmployeeByLastName()
        {
            return await _employeeRepository.GetEmployeeByLastName();
        }
    }
}
