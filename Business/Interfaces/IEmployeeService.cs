﻿using DTOModels;
using FirstWebApiApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstWebApiApp.Business
{
    public interface IEmployeeService
    {
        Task<List<EmployeeDTO>> GetAllEmployees();
        Task<EmployeeDTO> CreateEmployee(EmployeeDTO employee);
        Task<object> GetEmployeeByLastName();
        Task<EmployeeDTO> GetEmployeeById(string id);
    }
}
