﻿using FirstWebApiApp.Models;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace FirstWebApiApp.Utils.Helper
{
    public class AuthHelper
    {
        public static string GenerateToken(UserNew user)
        {
            string tokenString = String.Empty;
            if (user != null)
            {
                try
                {
                    string userID = EncryptionUtility.Encrypt(user.Id.ToString());
                    string userNumber = EncryptionUtility.Encrypt(user.PhoneNumber);
                    Claim[] claims = new Claim[]
                    {
                        new Claim("UserID", userID),
                        new Claim("UserNumber", userNumber),
                        new Claim("FirstName", user.FirstName),
                        new Claim("LastName", user.LastName)
                    };
                    // Key for creation/validation token
                    var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Startup.TokenKey));
                    // Create Token credentials
                    var signInCred = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature);
                    // Lifetime , user info...
                    var token = new JwtSecurityToken(
                        issuer: Startup.Issuer,
                        audience: Startup.Audience,
                        expires: DateTime.Now.AddHours(24),
                        claims: claims,
                        signingCredentials: signInCred
                        );
                    tokenString = new JwtSecurityTokenHandler().WriteToken(token);
                }
                catch (Exception ex)
                {

                     return ex.Message;
                }
            }
            else
            {
                return null;
            }

            return tokenString;
        }
    }
}
