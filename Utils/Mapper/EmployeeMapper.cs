﻿using AutoMapper;
using DTOModels;
using FirstWebApiApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstWebApiApp.Utils
{
    public class EmployeeMapper : Profile
    {
        public EmployeeMapper()
        {
            CreateMap<Employee, EmployeeDTO>()
                .ForMember(s => s.emp_id, opt => opt.MapFrom(s => EncryptionUtility.Encrypt(s.EmpId.ToString())))
                .ForMember(s => s.f_name, opt => opt.MapFrom(s => s.Fname))
                .ForMember(s => s.l_name, opt => opt.MapFrom(s => s.Lname))
                .ForMember(s => s.b_date, opt => opt.MapFrom(s => s.Bdate))
                .ForMember(s => s.department_id_fk, opt => opt.MapFrom(s => s.DepartmentIdFk))
                .ForMember(s => s.supervisor_employee_id, opt => opt.MapFrom(s => s.SupervisorEmployeeId));

            CreateMap<EmployeeDTO, Employee>()
                .ForMember(s => s.Fname, opt => opt.MapFrom(s => s.f_name))
                .ForMember(s => s.Lname, opt => opt.MapFrom(s => s.l_name))
                .ForMember(s => s.Bdate, opt => opt.MapFrom(s => s.b_date))
                .ForMember(s => s.DepartmentIdFk, opt => opt.MapFrom(s => s.department_id_fk))
                .ForMember(s => s.SupervisorEmployeeId, opt => opt.MapFrom(s => s.supervisor_employee_id));
        }
    }
}
