﻿using DTOModels;
using FirstWebApiApp.Data.Interfaces;
using FirstWebApiApp.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstWebApiApp.Data.Repository
{
    public class ProjectRepository : IProjectRepository
    {        
        public ProjectRepository()
        {

        }

        public async Task<List<ProjectDTO>> GetAllProjects()
        {
            using (TelecomContext db = new TelecomContext())
            {
                List<Project> allProjects = db.Projects.ToList();

                List<ProjectDTO> projects_dto = new List<ProjectDTO>();

                allProjects.ForEach(x =>
                {
                    projects_dto.Add(
                        new ProjectDTO 
                        { 
                            project_id = x.ProjectId,
                            project_name = x.ProjectName,
                            project_location = x.ProjectLocation,
                            project_number = x.ProjectNumber,
                            dep_id_fk = x.DepartmentIdFk
                        });
                });

                return projects_dto;
            }            
        }

        public async Task<ProjectDTO> GetProjectById(int id)
        {
            using (TelecomContext db = new TelecomContext())
            {
                Project project = db.Projects.Where(p => p.ProjectId == id).FirstOrDefault();
                if(project != null)
                {
                    return new ProjectDTO { 
                        project_id = project.ProjectId,
                        project_name = project.ProjectName,
                        project_location = project.ProjectLocation,
                        project_number = project.ProjectNumber,
                        dep_id_fk = project.DepartmentIdFk
                    };
                }
                else
                {
                    return new ProjectDTO();
                }
            }
        }

        public async Task<Project> UpdateProject(Project project)
        {
            using (TelecomContext db = new TelecomContext())
            {
                Project existingProject = db.Projects.Where(p => p.ProjectId == project.ProjectId).FirstOrDefault();
                if (existingProject != null)
                {
                    existingProject.ProjectLocation = project.ProjectLocation;
                    existingProject.ProjectName = project.ProjectName;
                    existingProject.ProjectNumber = project.ProjectNumber;
                    existingProject.DepartmentIdFk = project.DepartmentIdFk;

                    db.SaveChanges();
                    return existingProject;
                }
                else
                {
                    return new Project();
                }
            }
        }

        public async Task<ProjectDTO> CreateProject(ProjectDTO projectDTO)
        {
            using (TelecomContext db = new TelecomContext())
            {               
                if (projectDTO != null)
                {
                    Project project = new Project();
                    if (!string.IsNullOrEmpty(projectDTO.project_name))
                    {
                        project.ProjectName = projectDTO.project_name;
                    }
                    if (!string.IsNullOrEmpty(projectDTO.project_location))
                    {
                        project.ProjectLocation = projectDTO.project_location;
                    }
                    if (projectDTO.project_number > 0)
                    {
                        project.ProjectNumber = projectDTO.project_number;
                    }
                    if (projectDTO.project_id > 0)
                    {
                        project.ProjectId = projectDTO.project_id;
                    }
                    if(projectDTO.dep_id_fk > 0)
                    {
                        // proverka so LINQ dali FK postoi
                        Department exestingDepartment = db.Departments.Where(d => d.DepartmentId == projectDTO.dep_id_fk).FirstOrDefault();

                        if (exestingDepartment != null)
                        {
                            project.DepartmentIdFk = projectDTO.dep_id_fk;
                        }
                        else
                        {
                            throw new KeyNotFoundException($"Expected record for department key {projectDTO.dep_id_fk} not found.");
                        }
                    }

                    db.Projects.Add(project);
                    db.SaveChanges();
                    return projectDTO;
                }
                else
                {
                    return new ProjectDTO();
                }
            }
        }

        public async Task<Project> DeleteProject(int id)
        {
            using (TelecomContext db = new TelecomContext())
            {
                Project project = db.Projects.Include(x => x.WorksOns)
                                             .Where(p => p.ProjectId == id)                                           
                                             .FirstOrDefault();
                if (project != null)
                {
                    try
                    {
                        for (int i = 0; i < project.WorksOns.Count; i++)
                        {
                            var workson = project.WorksOns.ToList()[i];
                            project.WorksOns.Remove(workson);
                        }
                        db.SaveChanges();

                        db.Projects.Remove(project);
                        await db.SaveChangesAsync();
                        return project;
                    }
                    catch(Exception ex)
                    {
                        return new Project();
                    }
                    
                }
                else
                {
                    return new Project();
                }
            }
        }

        public async Task<List<ProjectDTO>> GetProjectsByName(string projectName)
        {
            using (TelecomContext db = new TelecomContext())
            {   // Method Syntax
                /* List<Project> projects = db.Projects.Where(p => p.ProjectName.ToLower()
                                                                  .Contains(projectName.ToLower())
                                                            ).ToList();*/

                // Query Syntax
                List<Project> projects = (from project in db.Projects.ToList()
                                         where project.ProjectName.ToLower().Contains(projectName.ToLower())
                                         select project).ToList();

                List<ProjectDTO> projects_dto = new List<ProjectDTO>();

                projects.ForEach(x =>
                {
                    projects_dto.Add(
                        new ProjectDTO
                        {
                            project_id = x.ProjectId,
                            project_name = x.ProjectName,
                            project_location = x.ProjectLocation,
                            project_number = x.ProjectNumber,
                            dep_id_fk = x.DepartmentIdFk
                        });
                });


                if (projects_dto != null)
                {
                    return projects_dto;
                }
                else
                {
                    return new List<ProjectDTO> { new ProjectDTO()};
                }
            }
        }

        public async Task<object> GetProjectsByDepartment(string department)
        {
            using (TelecomContext db = new TelecomContext())
            {  
                // Query Syntax
                var projects = (from project in db.Projects.ToList()
                                            join dep in db.Departments
                                            on project.DepartmentIdFk equals dep.DepartmentId
                                            where dep.DepartmentName.ToLower().Contains(department.ToLower())
                                            orderby project.ProjectName descending
                                            select new { 
                                                project.ProjectId,
                                                project.ProjectName,
                                                project.ProjectNumber,
                                                project.ProjectLocation,
                                                project.DepartmentIdFk
                                            }
                                            ).ToList();

                List<ProjectDTO> projects_dto = new List<ProjectDTO>();

                projects.ForEach(x =>
                {
                    projects_dto.Add(
                        new ProjectDTO
                        {
                            project_id = x.ProjectId,
                            project_name = x.ProjectName,
                            project_location = x.ProjectLocation,
                            project_number = x.ProjectNumber,
                            dep_id_fk = x.DepartmentIdFk
                        });
                });

                if (projects_dto != null)
                {
                    return projects_dto;
                }
                else
                {
                    return new List<ProjectDTO> { new ProjectDTO() };
                }
            }
        }
        
        public async Task<object> GetNumberOfProjectsByDepartment()
        {
            using (TelecomContext db = new TelecomContext())
            {
                // Query Syntax
                var projects = (from project in db.Projects.ToList()
                               join dep in db.Departments
                               on project.DepartmentIdFk equals dep.DepartmentId
                               group project by dep.DepartmentName into g
                               select new { Department = g.Key, NumberOfProjects = g.Count() }).ToList();


                if (projects != null)
                {
                    return projects;
                }
                else
                {
                    return new List<Project> { new Project() };
                }
            }
        }
    }
}
