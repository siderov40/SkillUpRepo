﻿using AutoMapper;
using DTOModels;
using FirstWebApiApp.Data.Interfaces;
using FirstWebApiApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstWebApiApp.Data.Repository
{
    public class EmployeeRepository : IEmployeeRepository
    {
        public async Task<Employee> CreateEmployee(Employee employeeNew)
        {
            using (TelecomContext db = new TelecomContext())
            {
                if (employeeNew != null)
                {
                    Employee employee = new Employee();
                    if (!string.IsNullOrEmpty(employeeNew.Fname))
                    {
                        employee.Fname = employeeNew.Fname;
                    }
                    if (!string.IsNullOrEmpty(employeeNew.Lname))
                    {
                        employee.Lname = employeeNew.Lname;
                    }
                    if (!string.IsNullOrEmpty(employeeNew.Ssn))
                    {
                        Employee existingEmployee = db.Employees.Where(e => e.Ssn == employeeNew.Ssn).FirstOrDefault();

                        if (existingEmployee == null)
                        {
                            employee.Ssn = employeeNew.Ssn;
                        }
                        else
                        {                           
                            throw new ArgumentException($"Ssn record is unique. This {employeeNew.Ssn} already exists!");
                        }
                    }
                    if (!string.IsNullOrEmpty(employeeNew.Address))
                    {
                        employee.Address = employeeNew.Address;
                    }
                    if (!string.IsNullOrEmpty(employeeNew.Gender))
                    {
                        employee.Gender = employeeNew.Gender;
                    }
                    if (employeeNew.Salary > 0)
                    {
                        employee.Salary = employeeNew.Salary;
                    }
                    DateTime temp;
                    if (DateTime.TryParse(employeeNew.Bdate.ToString(), out temp))
                    {
                        employee.Bdate = employeeNew.Bdate;
                    }
                    if (employeeNew.SupervisorEmployeeId > 0)
                    {
                        Employee existingEmployee = db.Employees.Where(e => e.EmpId == employeeNew.SupervisorEmployeeId).FirstOrDefault();

                        if (existingEmployee != null)
                        {
                            employee.SupervisorEmployeeId = employeeNew.SupervisorEmployeeId;
                        }
                        else
                        {
                            throw new KeyNotFoundException($"Expected record for employee supervisor key {employeeNew.SupervisorEmployeeId} not found.");
                        }
                    }
                    if (employeeNew.DepartmentIdFk > 0)
                    {
                        Department exestingDepartment = db.Departments.Where(d => d.DepartmentId == employeeNew.DepartmentIdFk).FirstOrDefault();

                        if (exestingDepartment != null)
                        {
                            employee.DepartmentIdFk = employeeNew.DepartmentIdFk;
                        }
                        else
                        {
                            throw new KeyNotFoundException($"Expected record for department key {employeeNew.DepartmentIdFk} not found.");
                        }
                    }

                    db.Employees.Add(employee);
                    db.SaveChanges();
                    return employee;
                }
                else
                {
                    return new Employee();
                }
            }
        }

        public async Task<List<Employee>> GetAllEmployees()
        {
            using (TelecomContext db = new TelecomContext())
            {
                List<Employee> allEmployee = db.Employees.ToList();

                return allEmployee;
            }
        }

        public async Task<Employee> GetEmployeeById(int id)
        {
            using (TelecomContext db = new TelecomContext())
            {
                Employee existingEmployee = db.Employees.Where(e => e.EmpId == id).FirstOrDefault();

               

                if (existingEmployee != null)
                {
                    return existingEmployee;
                }
                else
                {
                    return new Employee();
                }
            }
        }

        public async Task<object> GetEmployeeByLastName()
        {
            using (TelecomContext db = new TelecomContext())
            {
                // Query Syntax
                var employees = (from employee in db.Employees.ToList()
                                group employee by employee.Lname into g
                                select new
                                {
                                    LastName = g.Key,
                                    NumberOfLastNames = g.Count()
                                }
                                ).ToList();


                if (employees != null)
                {
                    return employees;
                }
                else
                {
                    return new List<Employee> { new Employee() };
                }
            }
        }
    }
}
