﻿using FirstWebApiApp.Data.Interfaces;
using FirstWebApiApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstWebApiApp.Data.Repository
{
    public class UserRepository : IUserRepository
    {
        List<User> users = new List<User>();

        public UserRepository()
        {
            users.AddRange(new List<User>
            {
                new User {Id = 0, FirstName = "Tom", LastName = "Flinn", AccStatus = true},
                new User {Id = 1, FirstName = "Fionna", LastName = "Breckins", AccStatus = true},
                new User {Id = 2, FirstName = "Tina", LastName = "Coleman", AccStatus = false}
            });
        }

        #region POST Action
        public async Task<string> CreateUser(User user)
        {
            users.Add(user);

            return await Task.Run(() => "User successfully created!");
        }
        #endregion

        #region DELETE Action
        public async Task<string> DeleteUser(int id)
        {
            int userIndex = users.FindIndex(s => s.Id == id);

            if (userIndex != -1)
            {
                users.RemoveAt(userIndex);

                return await Task.Run(() => $"User with ID {id} is successfully deleted!");
            }

            return await Task.Run(() => $"User with ID {id} doesn't exist!");
        }
        #endregion

        #region GET Actions
        public async Task<List<User>> GetAllUsers()
        {
            return await Task.Run(() => users);
        }

        public async Task<User> GetUserById(int id)
        {
            User user = users.Where(s => s.Id == id).FirstOrDefault();

            if(user != null)
            {
                return await Task.Run(() => user);
            }

            return await Task.Run(() => new User());
        }
        #endregion

        #region PUT Action
        public async Task<string> UpdateUser(User user, int id)
        {
            int userIndex = users.FindIndex(s => s.Id == id);

            if (userIndex != -1)
            {
                users.RemoveAt(userIndex);
                users.Insert(userIndex, user);

                return await Task.Run(() => "User successfully updated!");
            }

            return await Task.Run(() => "User not found!");
        }
        #endregion
    }
}
