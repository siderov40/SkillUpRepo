﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirstWebApiApp.Data.Interfaces;
using FirstWebApiApp.Models;

namespace FirstWebApiApp.Data.Repository
{
    public class UsersRepository : IUsersRepository
    {
        List<Users> users = new List<Users>();

        public UsersRepository()
        {
            #region STATIC DATA
            users.AddRange(new List<Users> {
                new Users
                {
                    Id = 1,
                    FirstName = "Mike",
                    LastName="Jones",
                    IsProfileCompleted=true
                },
                new Users
                {
                    Id = 2,
                    FirstName = "Sarah",
                    LastName="",
                    IsProfileCompleted=false
                },
               new Users
                {
                    Id = 3,
                    FirstName = "Jon",
                    LastName="Bond",
                    IsProfileCompleted=true
                }
            });
            #endregion
        }
        #region GET CALLS
        public async Task<List<Users>> getAllUsers()
        {
            return users;
        }
        public async Task<Users> getUserById(int id)
        {
            Users user = users.Where(s => s.Id == id).FirstOrDefault();
            if (user != null)
            {
                return user;
            }
            return new Users();
        }
        #endregion

        #region POST CALLS 
        public async Task<string> createNewUser(Users user)
        {
            users.Add(user);

            return "User is succefull created";
        }
        #endregion

        #region PUT CALLS 
        public async Task<string> updateUser(Users user, int id)
        {
            int? index = users.FindIndex(s => s.Id == id);

            if (index != -1)
            {
                users.RemoveAt((int)index);
                users.Insert((int)index, user);
                return "User successfully updated";
            }

            return "User not found";

        }
        #endregion

        #region DELETE CALLS
        public async Task<string> deleteUser(int id)
        {
            int index = users.FindIndex(s => s.Id == id);
            if (index != -1)
            {
                users.RemoveAt(index);
                return "User with id " + id + "is successfully deleted";
            }
            return "User with id " + id + " doesn't exist";
        }

      
        #endregion
    }
}
