﻿using DTOModels;
using FirstWebApiApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstWebApiApp.Data.Interfaces
{
    public interface IProjectRepository
    {
        Task<List<ProjectDTO>> GetAllProjects();
        Task<ProjectDTO> GetProjectById(int id);
        Task<Project> UpdateProject(Project project);
        Task<ProjectDTO> CreateProject(ProjectDTO project);
        Task<Project> DeleteProject(int id);
        Task<List<ProjectDTO>> GetProjectsByName(string projectName);
        Task<object> GetProjectsByDepartment(string department);
        Task<object> GetNumberOfProjectsByDepartment();
    }
}
