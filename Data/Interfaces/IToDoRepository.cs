﻿using FirstWebApiApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstWebApiApp.Data.Interfaces
{
    public interface IToDoRepository
    {
        Task<List<ToDoItem>> GetAllItems();
        Task<ToDoItem> GetItemById(int id);
        Task<string> CreateNewItem(ToDoItem toDoItem);
        Task<string> UpdateItem(ToDoItem toDoItem, int id);
        Task<string> DeleteItem(int id);
    }
}
