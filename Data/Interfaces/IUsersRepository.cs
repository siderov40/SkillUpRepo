﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirstWebApiApp.Models;

namespace FirstWebApiApp.Data.Interfaces
{
    public interface IUsersRepository
    {
        Task<List<Users>> getAllUsers();
        Task<Users> getUserById(int id);
        Task<string> createNewUser(Users user);
        Task<string> updateUser(Users user, int id);
        Task<string> deleteUser(int id);




    }
}
