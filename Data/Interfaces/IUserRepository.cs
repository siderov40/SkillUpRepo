﻿using FirstWebApiApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstWebApiApp.Data.Interfaces
{
    public interface IUserRepository
    {
        Task<List<User>> GetAllUsers();
        Task<User> GetUserById(int id);
        Task<string> CreateUser(User user);
        Task<string> UpdateUser(User user, int id);
        Task<string> DeleteUser(int id);
    }
}
