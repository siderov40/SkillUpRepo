﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using FirstWebApiApp.Data.Interfaces;
using FirstWebApiApp.Data.Repository;
using FirstWebApiApp.Models;
using System.Net;

namespace FirstWebApiApp.Controllers
{
    [Route("users")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        public readonly static IUsersRepository usersRepository = new UsersRepository();

        #region GET CALLS
        #region GET ALL USERS
        [HttpGet("get/all")]
        public async Task<ActionResult> getAllUsers()
        {
            try
            {
                List<Users> reponse_repo = await usersRepository.getAllUsers();
                return new ObjectResult(new { message = "success", statusCode = HttpStatusCode.OK, response = reponse_repo });
            }
            catch (Exception ex)
            {
                return new NotFoundObjectResult(new { message = ex.Message, statusCode = HttpStatusCode.InternalServerError });
            }
        }
        #endregion

        #region GET USER BY ID
        [HttpGet("get/by/{id}")]
        public async Task<IActionResult> getUsersById(int id)
        {
            try
            {
                Users response_repo = await usersRepository.getUserById(id);

                return new ObjectResult(new { message = "success", statusCode = HttpStatusCode.OK, response = response_repo });
            }
            catch (Exception ex)
            {
                return new NotFoundObjectResult(new { message = ex.Message, statusCode = HttpStatusCode.InternalServerError });
            }

        }

        #endregion

        #endregion

        #region POST CALLS 

        #region CREATE NEW USER
        [HttpPost("create")]
        public async Task<IActionResult> createNewUser([FromBody] Users user)
        {
            try
            {
                if (user == null)
                {
                    return new NotFoundObjectResult(new { message = "User is null", statusCode = HttpStatusCode.InternalServerError });
                }
                string reponse_repo = await usersRepository.createNewUser(user);
                return new ObjectResult(reponse_repo);
            }
            catch (Exception ex)
            {
                return new NotFoundObjectResult(new { message = ex.Message, statusCode = HttpStatusCode.InternalServerError });
            }
        }
        #endregion


        #endregion

        #region PUT CALLS
        [HttpPut("update/{id}")]
        public async Task<IActionResult> updateUser([FromBody] Users user, int id)
        {
            try
            {
                if (user == null)
                {
                    return new NotFoundObjectResult(new { message = "User is null", statusCode = HttpStatusCode.InternalServerError });
                }
                string response_repo = await usersRepository.updateUser(user, id);
                return new ObjectResult(new { message = "success", statusCode = HttpStatusCode.OK, response = response_repo });
            }
            catch (Exception ex)
            {
                return new NotFoundObjectResult(new { message = ex.Message, statusCode = HttpStatusCode.InternalServerError });
            }
        }
        #endregion

        #region DELETE CALL  
        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> deleteUser(int id)
        {
            try
            {
                string response_repo = await usersRepository.deleteUser(id);
                return new ObjectResult(new { message = "success", statusCode = HttpStatusCode.OK, response = response_repo });
            }
            catch (Exception ex)
            {
                return new NotFoundObjectResult(new { message = ex.Message, statusCode = HttpStatusCode.InternalServerError });
            }
        }
        #endregion
    }
}
