﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirstWebApiApp.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;
using System.Net;
using DTOModels;
using FirstWebApiApp.Utils.Helper;

namespace FirstWebApiApp.Controllers
{
    [Route("account")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly UserManager<UserNew> userMenager;
        private readonly SignInManager<UserNew> signInMenager;
        private readonly TelecomContext db;

        public AccountController(UserManager<UserNew> _userMenager,
            SignInManager<UserNew> _signInMenager,
            TelecomContext _db
            )
        {
            userMenager = _userMenager;
            signInMenager = _signInMenager;
            db = _db;
        }

        [HttpPost("register")]
        public async Task<IActionResult> registerUser(UserNewDTO _user)
        {
            try
            {
                UserNew user = new UserNew()
                {
                    UserName = _user.username,
                    FirstName = _user.first_name,
                    LastName = _user.last_name
                };

                IdentityResult result = await userMenager.CreateAsync(user, _user.password);

                if (result.Succeeded)
                {
                    return new ObjectResult(new { message = 200, statusCode = HttpStatusCode.OK, response = "User is created"});
                }
                else
                {
                    return new ObjectResult(new { message = 200, statusCode = HttpStatusCode.OK, response = "Failed registration" });
                }
            }
            catch (Exception ex)
            {

                return new NotFoundObjectResult(new { message = ex.Message, statusCode = HttpStatusCode.InternalServerError });
            }
        } 

        [HttpPost("login")]
        public async Task<IActionResult> loginUser(LoginUserDTO _user)
        {
            try
            {
                SignInResult result = await signInMenager.PasswordSignInAsync(_user.user_name, _user.password, true, false);

                if (result.Succeeded)
                {
                    return new ObjectResult(new { message = 200, statusCode = HttpStatusCode.OK, response = "User is created"});
                }
                else
                {
                    return new ObjectResult(new { message = 200, statusCode = HttpStatusCode.OK, response = result });
                }
            }
            catch (Exception ex)
            {

                return new ObjectResult(new { message = ex.Message, statusCode = HttpStatusCode.InternalServerError});
            }
        }

        [HttpPost("/createToken")]
        public async Task<IActionResult> createToken([FromBody] UserNewDTO user)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string token = String.Empty;

                    UserNew User = await userMenager.FindByNameAsync(user.username);
                    if (User != null)
                    {
                        var signInResult = await signInMenager.CheckPasswordSignInAsync(User, user.password, false);

                        if (signInResult.Succeeded)
                        {
                            token = AuthHelper.GenerateToken(User);
                        }
                        else
                        {
                            return new NotFoundObjectResult( new { message = "401",
                            statusCode = HttpStatusCode.Unauthorized, response = "Token is not created!"});
                        }
                    }

                    return new ObjectResult(new { message = 200, statusCode = HttpStatusCode.OK, response = token});
                }

                return BadRequest();
            }
            catch (Exception ex)
            {

                throw;
            }
        }
            
    }
}
