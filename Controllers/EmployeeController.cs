﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using DTOModels;
using FirstWebApiApp.Business;
using FirstWebApiApp.Data.Interfaces;
using FirstWebApiApp.Data.Repository;
using FirstWebApiApp.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace FirstWebApiApp.Controllers
{
    [Route("employee")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeService _emloyeeService;
        private readonly ILogger<EmployeeController> _logger;
        public EmployeeController(IEmployeeService employeeService, ILogger<EmployeeController> logger)
        {
            _emloyeeService = employeeService;
            _logger = logger;
        }

        #region GET CALLS

        /// <summary>
        /// Get list of all employees
        /// </summary>
        /// <remarks>
        ///     GET/employee/get/all
        /// </remarks>
        /// <returns>Get list of all employees</returns>
        /// <response code = "200">
        ///     Return list of all employees
        ///     or empty list if no employees were found
        /// </response>
        [HttpGet("get/all")]
        public async Task<IActionResult> GetAllEmployees()
        {
            try
            {
                _logger.LogInformation("ENTERING in the EmployeeController GetAllEmployees!");
                List<EmployeeDTO> response_repo = await _emloyeeService.GetAllEmployees();
                _logger.LogInformation("RETURN response in the EmployeeController GetAllEmployees!");
                return new ObjectResult(new { message = "success", HttpStatusCode.OK, response = response_repo });
            }
            catch (Exception ex)
            {
                _logger.LogInformation("ERROR - EmployeeController - GetAllEmployees! " + ex.Message);
                return new ObjectResult(new { message = ex.Message, HttpStatusCode.InternalServerError });
            }
        }

        /// <summary>
        /// Get employee by Id
        /// </summary>
        /// <remarks>
        ///     GET/employee/get/by/{id}
        /// </remarks>
        /// <returns>Get employee by Id</returns>
        /// <response code = "200">
        ///     Return object of employee by Id
        ///     or empty employee object if not found
        /// </response>
        [HttpGet("get/by/{id}")]
        public async Task<IActionResult> GetEmployeeById(string id)
        {
            try
            {
                _logger.LogInformation("ENTERING in the EmployeeController GetEmployeeById for id=" + id);
                EmployeeDTO response_repo = await _emloyeeService.GetEmployeeById(id);
                if (response_repo.emp_id != "")
                {
                    _logger.LogInformation("RETURN response in the EmployeeController GetEmployeeById for id=" + id);
                    return new ObjectResult(new { message = "success", HttpStatusCode.OK, response = response_repo });
                }
                else
                {
                    _logger.LogInformation("ERROR - EmployeeController - GetEmployeeById! for id=" + id + "Error: Not found project");
                    return new NotFoundObjectResult("Not found project");
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation("ERROR - EmployeeController - GetEmployeeById! for id=" + id + "Error: " + ex.Message);
                return new ObjectResult(new { message = ex.Message, HttpStatusCode.InternalServerError });
            }
        }

        /// <summary>
        /// Get list of employees grouped by last name
        /// </summary>
        /// <remarks>
        ///     GET/employee/get/groupedbylname
        /// </remarks>
        /// <returns>Get list of employees grouped by last name with number of employees</returns>
        /// <response code = "200">
        ///     Return list of employees grouped by last name
        ///     or empty list for no employees
        /// </response>
        [HttpGet("get/groupedbylname")]
        public async Task<IActionResult> GetEmployeeByLastName()
        {
            try
            {
                _logger.LogInformation("ENTERING in the EmployeeController GetEmployeeByLastName");
                object response_repo = await _emloyeeService.GetEmployeeByLastName();
                _logger.LogInformation("RETURN response in the EmployeeController GetEmployeeByLastName");
                return new ObjectResult(new { message = "success", HttpStatusCode.OK, response = response_repo });
            }
            catch (Exception ex)
            {
                _logger.LogInformation("ERROR - EmployeeController - GetEmployeeByLastName!" + "Error: " + ex.Message);
                return new ObjectResult(new { message = ex.Message, HttpStatusCode.InternalServerError });
            }
        }

        #endregion

        #region POST CALLS

        /// <summary>
        /// Create Employee
        /// </summary>
        /// <remarks>
        ///     POST/employee/create
        ///     
        /// {
        ///    "f_name": "John",
        ///    "l_name": "Doe",
        ///    "ssn": "123356789",
        ///    "address": "Address 123",
        ///    "gender": "M",
        ///    "salary": 200000,
        ///    "b_date": "2000-12-22T12:33:11.531Z",
        ///    "supervisor_employee_id": 8,
        ///    "department_id_fk": 2
        /// }
        /// 
        /// Mandatory fields:
        /// "f_name",
        /// "l_name"
        /// </remarks>
        /// <returns>Employee object</returns>
        /// <response code = "200">
        ///     Return Employee object
        ///     and empty employee object for empty data
        /// </response>
        /// <response code = "500" >
        ///     Expected record for employee supervisor key not found
        /// </response>
        /// <response code = "500" >
        ///     Expected record for department key not found
        /// </response>
        /// <response code = "500" >
        ///     Ssn record is unique. This record already exists
        /// </response>
        [HttpPost("create")]
        public async Task<IActionResult> CreateEmployee([FromBody] EmployeeDTO employee)
        {
            try
            {
                _logger.LogInformation("ENTERING in the EmployeeController CreateEmployee");
                EmployeeDTO response_repo = await _emloyeeService.CreateEmployee(employee);
                if (!string.IsNullOrEmpty(response_repo.f_name))
                {
                    _logger.LogInformation("RETURN response in the EmployeeController CreateEmployee");
                    return new ObjectResult(new { message = "success", HttpStatusCode.OK, response = response_repo });
                }
                else
                {
                    _logger.LogInformation("ERROR - EmployeeController - CreateEmployee!" + "Fname is null");
                    return new ObjectResult(new { message = "error", HttpStatusCode.InternalServerError, response = response_repo });
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation("ERROR - EmployeeController - CreateEmployee!" + "Error: " + ex.Message);
                return new ObjectResult(new { message = ex.Message, HttpStatusCode.InternalServerError });
            }
        }

        #endregion
    }
}
