﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using DTOModels;
using FirstWebApiApp.Data.Interfaces;
using FirstWebApiApp.Data.Repository;
using FirstWebApiApp.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FirstWebApiApp.Controllers
{
    [Route("projects")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        public readonly static IProjectRepository projectRepository = new ProjectRepository();

        #region GET CALLS

        #region GET ALL

        [HttpGet("get/all")]
        public async Task<IActionResult> GetAllProjects()
        {
            try
            {
                List<ProjectDTO> response_repo = await projectRepository.GetAllProjects();
                return new ObjectResult(new { message = "success", HttpStatusCode.OK, response = response_repo });
            }
            catch (Exception ex)
            {
                return new ObjectResult(new { message = ex.Message, HttpStatusCode.InternalServerError });
            }
        }

        #endregion

        #region GET BY ID

        [HttpGet("get/by/{id}")]
        public async Task<IActionResult> GetProjectById(int id)
        {
            try
            {
                ProjectDTO response_repo = await projectRepository.GetProjectById(id);
                if(response_repo.project_id != 0)
                {
                    return new ObjectResult(new { message = "success", HttpStatusCode.OK, response = response_repo });
                }
                else
                {
                    return new NotFoundObjectResult("Not found project");
                }
            }
            catch (Exception ex)
            {
                return new ObjectResult(new { message = ex.Message, HttpStatusCode.InternalServerError });
            }
        }

        #endregion

        #region LINQ REQUESTS

        /// <summary>
        /// Get projects by project name
        /// </summary>
        /// <remarks>
        ///     GET/projects/get/name
        /// </remarks>
        /// <param name="name">Name of the project</param>
        /// <returns>List of projects by part of the project name</returns>
        /// <response code = "200">
        ///     Return list of projects for entered project name
        ///     and empty list if there are no project for the entered project name
        /// </response>
        [HttpGet("get/name/{name}")]
        public async Task<IActionResult> GetProjectsByName(string name)
        {
            try
            {
                List<ProjectDTO> response_repo = await projectRepository.GetProjectsByName(name);
                return new ObjectResult(new { message = "success", HttpStatusCode.OK, response = response_repo });
            }
            catch (Exception ex)
            {
                return new ObjectResult(new { message = ex.Message, HttpStatusCode.InternalServerError });
            }
        }

        /// <summary>
        /// Get projects by department
        /// </summary>
        /// <remarks>
        ///     GET/projects/get/department
        /// </remarks>
        /// <param name="department">Name of department</param>
        /// <returns>List of projects for entered department</returns>
        /// <response code = "200">
        ///     Return list of projects for entered department
        ///     and empty list if there are no project for the entered department
        /// </response>
        [HttpGet("get/department/{department}")]
        public async Task<IActionResult> GetProjectsByDepartment(string department)
        {
            try
            {
                object response_repo = await projectRepository.GetProjectsByDepartment(department);
                return new ObjectResult(new { message = "success", HttpStatusCode.OK, response = response_repo });
            }
            catch (Exception ex)
            {
                return new ObjectResult(new { message = ex.Message, HttpStatusCode.InternalServerError });
            }
        }

        /// <summary>
        /// Get number od projects grouped by department
        /// </summary>
        /// <remarks>
        ///     GET/projects/get/groupedbydepartment
        /// </remarks>
        /// <returns>List of departments with information od number of projects per department</returns>
        /// <response code = "200">
        ///     Return list of departments and number of projects per department
        ///     and empty list for no departments
        /// </response>
        [HttpGet("get/groupedbydepartment")]
        public async Task<IActionResult> GetNumberOfProjectsByDepartment()
        {
            try
            {
                object response_repo = await projectRepository.GetNumberOfProjectsByDepartment();
                return new ObjectResult(new { message = "success", HttpStatusCode.OK, response = response_repo });
            }
            catch (Exception ex)
            {
                return new ObjectResult(new { message = ex.Message, HttpStatusCode.InternalServerError });
            }
        }

        #endregion

        #endregion

        #region PUT CALLS

        [HttpPut("update")]
        public async Task<IActionResult> UpdateProject([FromBody] Project project)
        {
            try
            {
                Project response_repo = await projectRepository.UpdateProject(project);
                if (response_repo.ProjectId != 0)
                {
                    return new ObjectResult(new { message = "success", HttpStatusCode.OK, response = response_repo });
                }
                else
                {
                    return new NotFoundObjectResult("Not found project");
                }
            }
            catch (Exception ex)
            {
                return new ObjectResult(new { message = ex.Message, HttpStatusCode.InternalServerError });
            }
        }

        #endregion

        #region POST CALLS

        [HttpPost("create")]
        public async Task<IActionResult> CreateProject([FromBody] ProjectDTO project)
        {
            try
            {
                ProjectDTO response_repo = await projectRepository.CreateProject(project);
                if (!string.IsNullOrEmpty(response_repo.project_name))
                {
                    return new ObjectResult(new { message = "success", HttpStatusCode.OK, response = response_repo });
                }
                else
                {
                    return new ObjectResult(new { message = "error", HttpStatusCode.InternalServerError, response = response_repo });
                }
            }
            catch (Exception ex)
            {
                return new ObjectResult(new { message = ex.Message, HttpStatusCode.InternalServerError });
            }
        }

        #endregion

        #region DELETE CALLS

        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> DeleteProject(int id)
        {
            try
            {
                Project response_repo = await projectRepository.DeleteProject(id);
                if (response_repo.ProjectId != 0)
                {
                    return new ObjectResult(new { message = "success", HttpStatusCode.OK, response = response_repo });
                }
                else
                {
                    return new NotFoundObjectResult("Not found project");
                }
            }
            catch (Exception ex)
            {
                return new ObjectResult(new { message = ex.Message, HttpStatusCode.InternalServerError });
            }
        }

        #endregion
    }
}
