﻿using FirstWebApiApp.Data.Interfaces;
using FirstWebApiApp.Data.Repository;
using FirstWebApiApp.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace FirstWebApiApp.Controllers
{
    [ApiController]
    [Route("users")]
    public class UserController : ControllerBase
    {
        public readonly static IUserRepository userRepo = new UserRepository();

        #region GET Actions
        [HttpGet("all")]
        public async Task<IActionResult> GetAllUsers()
        {
            try
            {
                List<User> users = await userRepo.GetAllUsers();

                return new ObjectResult(new { message = "Successfully done.", statusCode = HttpStatusCode.OK, response = users });
            }
            catch (Exception ex)
            {
                return new NotFoundObjectResult(new { message = ex.Message, statusCode = HttpStatusCode.InternalServerError });
            }
        }

        [HttpGet("user/{id}")]
        public async Task<IActionResult> GetUserById(int id)
        {
            try
            {
                User user = await userRepo.GetUserById(id);

                return new ObjectResult(new { message = "Successfully done.", statusCode = HttpStatusCode.OK, response = user });
            }
            catch (Exception ex)
            {
                return new NotFoundObjectResult(new { message = ex.Message, statusCode = HttpStatusCode.InternalServerError });
            }
        }
        #endregion

        #region POST Action
        [HttpPost("new/user")]
        public async Task<IActionResult> CreateUser([FromBody] User user)
        {
            try
            {
                if (user == null)
                {
                    return new NotFoundObjectResult(new { message = "User could not be found.", statusCode = HttpStatusCode.InternalServerError });
                }

                string userModel = await userRepo.CreateUser(user);
                
                return new ObjectResult(userModel);
            }
            catch (Exception ex)
            {
                return new NotFoundObjectResult(new { message = ex.Message, statusCode = HttpStatusCode.InternalServerError });
            }
        }
        #endregion

        #region PUT Action
        [HttpPut("update/user/{id}")]
        public async Task<IActionResult> UpdateUser([FromBody] User user, int id)
        {
            try
            {
                if (user == null)
                {
                    return new NotFoundObjectResult(new { message = "User could not be found.", statusCode = HttpStatusCode.InternalServerError });
                }

                string userModel = await userRepo.UpdateUser(user, id);

                return new ObjectResult(new { message = "Successfully updated.", statusCode = HttpStatusCode.OK, response = userModel });
            }
            catch (Exception ex)
            {
                return new NotFoundObjectResult(new { message = ex.Message, statusCode = HttpStatusCode.InternalServerError });
            }
        }
        #endregion

        #region DELETE Action
        [HttpDelete("remove/user/{id}")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            try
            {
                string user = await userRepo.DeleteUser(id);

                return new ObjectResult(new { message = "Successfully deleted.", statusCode = HttpStatusCode.OK, response = user });
            }
            catch (Exception ex)
            {
                return new NotFoundObjectResult(new { message = ex.Message, statusCode = HttpStatusCode.InternalServerError });
            }
        }
        #endregion
    }
}
