﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FirstWebApiApp.Models
{
    public partial class Project
    {
        public Project()
        {
            WorksOns = new HashSet<WorksOn>();
        }

        public int ProjectId { get; set; }
        public string ProjectLocation { get; set; }
        public string ProjectName { get; set; }
        public int ProjectNumber { get; set; }
        public int? DepartmentIdFk { get; set; }

        public virtual Department DepartmentIdFkNavigation { get; set; }
        public virtual ICollection<WorksOn> WorksOns { get; set; }
    }
}
