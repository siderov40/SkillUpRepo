﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FirstWebApiApp.Models
{
    public partial class Dependent
    {
        public int DependentId { get; set; }
        public string DependentName { get; set; }
        public string Gender { get; set; }
        public string Relationship { get; set; }
        public int? EmployeeIdFk { get; set; }
        public DateTime Dbate { get; set; }

        public virtual Employee EmployeeIdFkNavigation { get; set; }
    }
}
