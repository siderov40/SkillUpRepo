﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FirstWebApiApp.Models
{
    public partial class DepartmentLocation
    {
        public int DepartmentLocationId { get; set; }
        public string DepartmentLocation1 { get; set; }
        public int? DepartmentIdFk { get; set; }

        public virtual Department DepartmentIdFkNavigation { get; set; }
    }
}
