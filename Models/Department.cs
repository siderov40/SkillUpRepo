﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FirstWebApiApp.Models
{
    public partial class Department
    {
        public Department()
        {
            DepartmentLocations = new HashSet<DepartmentLocation>();
            Employees = new HashSet<Employee>();
            Projects = new HashSet<Project>();
        }

        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public int DepartmentNumber { get; set; }
        public int? EmployeeIdFk { get; set; }
        public DateTime? MgrStartDate { get; set; }

        public virtual Employee EmployeeIdFkNavigation { get; set; }
        public virtual ICollection<DepartmentLocation> DepartmentLocations { get; set; }
        public virtual ICollection<Employee> Employees { get; set; }
        public virtual ICollection<Project> Projects { get; set; }
    }
}
