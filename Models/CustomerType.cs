﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FirstWebApiApp.Models
{
    public partial class CustomerType
    {
        public CustomerType()
        {
            CustomerAditionalServices = new HashSet<CustomerAditionalService>();
            Customers = new HashSet<Customer>();
        }

        public int CustomerTypeId { get; set; }
        public string CustomerType1 { get; set; }

        public virtual ICollection<CustomerAditionalService> CustomerAditionalServices { get; set; }
        public virtual ICollection<Customer> Customers { get; set; }
    }
}
