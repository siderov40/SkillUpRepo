﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FirstWebApiApp.Models
{
    public partial class MyCircle
    {
        public int MyCircleId { get; set; }
        public int? PhoneNumberOwnerId { get; set; }
        public int? PhoneNumberCircleId { get; set; }

        public virtual PhoneNumber PhoneNumberCircle { get; set; }
        public virtual PhoneNumber PhoneNumberOwner { get; set; }
    }
}
