﻿using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace FirstWebApiApp.Models
{
    public partial class TelecomContext : IdentityDbContext<UserNew>
    {
        public TelecomContext()
        {
        }

        public TelecomContext(DbContextOptions<TelecomContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AditionalService> AditionalServices { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<UserNew> Users { get; set; }
        public virtual DbSet<CustomerAditionalService> CustomerAditionalServices { get; set; }
        public virtual DbSet<CustomerType> CustomerTypes { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<DepartmentLocation> DepartmentLocations { get; set; }
        public virtual DbSet<Dependent> Dependents { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<MyCircle> MyCircles { get; set; }
        public virtual DbSet<PhoneNumber> PhoneNumbers { get; set; }
        public virtual DbSet<Project> Projects { get; set; }
        public virtual DbSet<SmsMessage> SmsMessages { get; set; }
        public virtual DbSet<SmsReciever> SmsRecievers { get; set; }
        public virtual DbSet<WorksOn> WorksOns { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            /*if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=DESKTOP-LGUJTTE;Database=SkillUP;Integrated Security=true;");
            }*/

            optionsBuilder.UseSqlServer("Server=DESKTOP-LGUJTTE;Database=SkillUP;Integrated Security=true;", builder =>
            {
                builder.EnableRetryOnFailure(5, TimeSpan.FromSeconds(10), null);
            });
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1251_CI_AS");

            modelBuilder.Entity<AditionalService>(entity =>
            {
                entity.HasKey(e => e.AditionalId)
                    .HasName("PK__Aditiona__084BC8DDD6CF7BE3");

                entity.Property(e => e.AditionalDescription)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AditionalName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UserNew>(entity =>
            {
                entity.Property(e => e.FirstName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.ToTable("Customer");

                entity.Property(e => e.CityAddress)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerName)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerSurname)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.StreetAddress)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CustomerType)
                    .WithMany(p => p.Customers)
                    .HasForeignKey(d => d.CustomerTypeId)
                    .HasConstraintName("FK__Customer__Custom__3E52440B");
            });

            modelBuilder.Entity<CustomerAditionalService>(entity =>
            {
                entity.ToTable("CustomerAditionalService");

                entity.Property(e => e.AditionalIdFk).HasColumnName("AditionalIdFK");

                entity.Property(e => e.CustomerTypeIdFk).HasColumnName("CustomerTypeIdFK");

                entity.HasOne(d => d.AditionalIdFkNavigation)
                    .WithMany(p => p.CustomerAditionalServices)
                    .HasForeignKey(d => d.AditionalIdFk)
                    .HasConstraintName("FK__CustomerA__Aditi__3F466844");

                entity.HasOne(d => d.CustomerTypeIdFkNavigation)
                    .WithMany(p => p.CustomerAditionalServices)
                    .HasForeignKey(d => d.CustomerTypeIdFk)
                    .HasConstraintName("FK__CustomerA__Custo__403A8C7D");
            });

            modelBuilder.Entity<CustomerType>(entity =>
            {
                entity.ToTable("CustomerType");

                entity.Property(e => e.CustomerTypeId).HasColumnName("CustomerTypeID");

                entity.Property(e => e.CustomerType1)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("CustomerType");
            });

            modelBuilder.Entity<Department>(entity =>
            {
                entity.ToTable("Department");

                entity.Property(e => e.DepartmentName)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.EmployeeIdFk).HasColumnName("employeeIdFK");

                entity.Property(e => e.MgrStartDate).HasColumnType("datetime");

                entity.HasOne(d => d.EmployeeIdFkNavigation)
                    .WithMany(p => p.Departments)
                    .HasForeignKey(d => d.EmployeeIdFk)
                    .HasConstraintName("FK__Departmen__emplo__3A81B327");
            });

            modelBuilder.Entity<DepartmentLocation>(entity =>
            {
                entity.Property(e => e.DepartmentIdFk).HasColumnName("DepartmentIdFK");

                entity.Property(e => e.DepartmentLocation1)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("DepartmentLocation");

                entity.HasOne(d => d.DepartmentIdFkNavigation)
                    .WithMany(p => p.DepartmentLocations)
                    .HasForeignKey(d => d.DepartmentIdFk)
                    .HasConstraintName("FK__Departmen__Depar__4222D4EF");
            });

            modelBuilder.Entity<Dependent>(entity =>
            {
                entity.ToTable("Dependent");

                entity.Property(e => e.Dbate)
                    .HasColumnType("datetime")
                    .HasColumnName("DBate");

                entity.Property(e => e.DependentName)
                    .IsRequired()
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.EmployeeIdFk).HasColumnName("EmployeeIdFK");

                entity.Property(e => e.Gender)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Relationship)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.EmployeeIdFkNavigation)
                    .WithMany(p => p.Dependents)
                    .HasForeignKey(d => d.EmployeeIdFk)
                    .HasConstraintName("FK__Dependent__Emplo__4316F928");
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.HasKey(e => e.EmpId)
                    .HasName("PK__Employee__AF2DBA791A43ECB1");

                entity.Property(e => e.EmpId).HasColumnName("EmpID");

                entity.Property(e => e.Address)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Bdate)
                    .HasColumnType("date")
                    .HasColumnName("BDate");

                entity.Property(e => e.DepartmentIdFk)
                    .HasColumnName("DepartmentIdFK")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Fname)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("FName");

                entity.Property(e => e.Gender)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Lname)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("LName");

                entity.Property(e => e.Ssn)
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.SupervisorEmployeeId).HasColumnName("SupervisorEmployeeID");

                entity.HasOne(d => d.DepartmentIdFkNavigation)
                    .WithMany(p => p.Employees)
                    .HasForeignKey(d => d.DepartmentIdFk)
                    .HasConstraintName("FK__Employees__Depar__3D5E1FD2");

                entity.HasOne(d => d.SupervisorEmployee)
                    .WithMany(p => p.InverseSupervisorEmployee)
                    .HasForeignKey(d => d.SupervisorEmployeeId)
                    .HasConstraintName("FK__Employees__Super__44FF419A");
            });

            modelBuilder.Entity<MyCircle>(entity =>
            {
                entity.ToTable("MyCircle");

                entity.Property(e => e.PhoneNumberCircleId).HasColumnName("PhoneNumberCircleID");

                entity.Property(e => e.PhoneNumberOwnerId).HasColumnName("PhoneNumberOwnerID");

                entity.HasOne(d => d.PhoneNumberCircle)
                    .WithMany(p => p.MyCirclePhoneNumberCircles)
                    .HasForeignKey(d => d.PhoneNumberCircleId)
                    .HasConstraintName("FK__MyCircle__PhoneN__46E78A0C");

                entity.HasOne(d => d.PhoneNumberOwner)
                    .WithMany(p => p.MyCirclePhoneNumberOwners)
                    .HasForeignKey(d => d.PhoneNumberOwnerId)
                    .HasConstraintName("FK__MyCircle__PhoneN__45F365D3");
            });

            modelBuilder.Entity<PhoneNumber>(entity =>
            {
                entity.Property(e => e.CustomerIdFk).HasColumnName("CustomerIdFK");

                entity.Property(e => e.PhoneNumber1)
                    .IsRequired()
                    .HasMaxLength(20)
                    .HasColumnName("PhoneNumber");

                entity.HasOne(d => d.CustomerIdFkNavigation)
                    .WithMany(p => p.PhoneNumbers)
                    .HasForeignKey(d => d.CustomerIdFk)
                    .HasConstraintName("FK__PhoneNumb__Custo__47DBAE45");
            });

            modelBuilder.Entity<Project>(entity =>
            {
                entity.ToTable("Project");

                entity.Property(e => e.DepartmentIdFk).HasColumnName("DepartmentIdFK");

                entity.Property(e => e.ProjectLocation)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.DepartmentIdFkNavigation)
                    .WithMany(p => p.Projects)
                    .HasForeignKey(d => d.DepartmentIdFk)
                    .HasConstraintName("FK__Project__Departm__48CFD27E");
            });

            modelBuilder.Entity<SmsMessage>(entity =>
            {
                entity.HasKey(e => e.SmsMessagesId)
                    .HasName("PK__SmsMessa__624C01E8B530D052");

                entity.Property(e => e.Content).IsRequired();

                entity.HasOne(d => d.PhoneNumberSender)
                    .WithMany(p => p.SmsMessages)
                    .HasForeignKey(d => d.PhoneNumberSenderId)
                    .HasConstraintName("FK__SmsMessag__Phone__49C3F6B7");
            });

            modelBuilder.Entity<SmsReciever>(entity =>
            {
                entity.HasKey(e => e.SmsReciever1)
                    .HasName("PK__SmsRecie__B40C085354BB1CE2");

                entity.ToTable("SmsReciever");

                entity.Property(e => e.SmsReciever1).HasColumnName("SmsReciever");

                entity.Property(e => e.PhoneNumberIdFk).HasColumnName("PhoneNumberIdFK");

                entity.Property(e => e.SmsMessagesIdFk).HasColumnName("SmsMessagesIdFK");

                entity.HasOne(d => d.PhoneNumberIdFkNavigation)
                    .WithMany(p => p.SmsRecievers)
                    .HasForeignKey(d => d.PhoneNumberIdFk)
                    .HasConstraintName("FK__SmsReciev__Phone__4AB81AF0");

                entity.HasOne(d => d.SmsMessagesIdFkNavigation)
                    .WithMany(p => p.SmsRecievers)
                    .HasForeignKey(d => d.SmsMessagesIdFk)
                    .HasConstraintName("FK__SmsReciev__SmsMe__4BAC3F29");
            });

            modelBuilder.Entity<WorksOn>(entity =>
            {
                entity.HasKey(e => e.WorkOnId)
                    .HasName("PK__WorksOn__B9F6C9F920FE43E4");

                entity.ToTable("WorksOn");

                entity.Property(e => e.EmployeeIdFk).HasColumnName("EmployeeIdFK");

                entity.Property(e => e.ProjectIdFk).HasColumnName("ProjectIdFK");

                entity.HasOne(d => d.EmployeeIdFkNavigation)
                    .WithMany(p => p.WorksOns)
                    .HasForeignKey(d => d.EmployeeIdFk)
                    .HasConstraintName("FK__WorksOn__Employe__4CA06362");

                entity.HasOne(d => d.ProjectIdFkNavigation)
                    .WithMany(p => p.WorksOns)
                    .HasForeignKey(d => d.ProjectIdFk)
                    .HasConstraintName("FK__WorksOn__Project__4D94879B");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
