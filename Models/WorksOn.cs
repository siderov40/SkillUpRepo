﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FirstWebApiApp.Models
{
    public partial class WorksOn
    {
        public int WorkOnId { get; set; }
        public int? ProjectIdFk { get; set; }
        public int? EmployeeIdFk { get; set; }
        public int Hours { get; set; }

        public virtual Employee EmployeeIdFkNavigation { get; set; }
        public virtual Project ProjectIdFkNavigation { get; set; }
    }
}
