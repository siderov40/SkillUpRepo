﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FirstWebApiApp.Models
{
    public partial class AditionalService
    {
        public AditionalService()
        {
            CustomerAditionalServices = new HashSet<CustomerAditionalService>();
        }

        public int AditionalId { get; set; }
        public string AditionalName { get; set; }
        public string AditionalDescription { get; set; }

        public virtual ICollection<CustomerAditionalService> CustomerAditionalServices { get; set; }
    }
}
