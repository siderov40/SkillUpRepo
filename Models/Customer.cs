﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FirstWebApiApp.Models
{
    public partial class Customer
    {
        public Customer()
        {
            PhoneNumbers = new HashSet<PhoneNumber>();
        }

        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerSurname { get; set; }
        public int AddressNumber { get; set; }
        public string CityAddress { get; set; }
        public string StreetAddress { get; set; }
        public int? CustomerTypeId { get; set; }

        public virtual CustomerType CustomerType { get; set; }
        public virtual ICollection<PhoneNumber> PhoneNumbers { get; set; }
    }
}
