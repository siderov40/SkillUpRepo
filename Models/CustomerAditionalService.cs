﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FirstWebApiApp.Models
{
    public partial class CustomerAditionalService
    {
        public int CustomerAditionalServiceId { get; set; }
        public int Price { get; set; }
        public int? CustomerTypeIdFk { get; set; }
        public int? AditionalIdFk { get; set; }

        public virtual AditionalService AditionalIdFkNavigation { get; set; }
        public virtual CustomerType CustomerTypeIdFkNavigation { get; set; }
    }
}
