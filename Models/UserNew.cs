﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstWebApiApp.Models
{
    public class UserNew : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
