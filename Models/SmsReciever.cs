﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FirstWebApiApp.Models
{
    public partial class SmsReciever
    {
        public int SmsReciever1 { get; set; }
        public int? PhoneNumberIdFk { get; set; }
        public int? SmsMessagesIdFk { get; set; }

        public virtual PhoneNumber PhoneNumberIdFkNavigation { get; set; }
        public virtual SmsMessage SmsMessagesIdFkNavigation { get; set; }
    }
}
