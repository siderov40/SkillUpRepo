﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FirstWebApiApp.Models
{
    public partial class PhoneNumber
    {
        public PhoneNumber()
        {
            MyCirclePhoneNumberCircles = new HashSet<MyCircle>();
            MyCirclePhoneNumberOwners = new HashSet<MyCircle>();
            SmsMessages = new HashSet<SmsMessage>();
            SmsRecievers = new HashSet<SmsReciever>();
        }

        public int PhoneNumberId { get; set; }
        public string PhoneNumber1 { get; set; }
        public int? CustomerIdFk { get; set; }

        public virtual Customer CustomerIdFkNavigation { get; set; }
        public virtual ICollection<MyCircle> MyCirclePhoneNumberCircles { get; set; }
        public virtual ICollection<MyCircle> MyCirclePhoneNumberOwners { get; set; }
        public virtual ICollection<SmsMessage> SmsMessages { get; set; }
        public virtual ICollection<SmsReciever> SmsRecievers { get; set; }
    }
}
