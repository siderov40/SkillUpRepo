﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FirstWebApiApp.Models
{
    public partial class Employee
    {
        public Employee()
        {
            Departments = new HashSet<Department>();
            Dependents = new HashSet<Dependent>();
            InverseSupervisorEmployee = new HashSet<Employee>();
            WorksOns = new HashSet<WorksOn>();
        }

        public int EmpId { get; set; }
        public string Fname { get; set; }
        public string Lname { get; set; }
        public string Ssn { get; set; }
        public string Address { get; set; }
        public string Gender { get; set; }
        public int? Salary { get; set; }
        public DateTime? Bdate { get; set; }
        public int? SupervisorEmployeeId { get; set; }
        public int? DepartmentIdFk { get; set; }

        public virtual Department DepartmentIdFkNavigation { get; set; }
        public virtual Employee SupervisorEmployee { get; set; }
        public virtual ICollection<Department> Departments { get; set; }
        public virtual ICollection<Dependent> Dependents { get; set; }
        public virtual ICollection<Employee> InverseSupervisorEmployee { get; set; }
        public virtual ICollection<WorksOn> WorksOns { get; set; }
    }
}
