﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FirstWebApiApp.Models
{
    public partial class SmsMessage
    {
        public SmsMessage()
        {
            SmsRecievers = new HashSet<SmsReciever>();
        }

        public int SmsMessagesId { get; set; }
        public string Content { get; set; }
        public int? PhoneNumberSenderId { get; set; }

        public virtual PhoneNumber PhoneNumberSender { get; set; }
        public virtual ICollection<SmsReciever> SmsRecievers { get; set; }
    }
}
