using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using FirstWebApiApp.Business;
using FirstWebApiApp.Data.Interfaces;
using FirstWebApiApp.Data.Repository;
using FirstWebApiApp.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

namespace FirstWebApiApp
{
    public class Startup
    {
        public static string Audience { get; set; }
        public static string TokenKey { get; set; }
        public static string ConnectionString { get; set; }
        public static string Issuer { get; set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            #region STATIC VARIABLES
            TokenKey = Configuration["TokenKey"];
            Issuer = Configuration["Issuer_Dev"];
            Audience = Configuration["Audience_Dev"];
            #endregion
			
			

            #region JWT AUTH
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Issuer,
                        ValidAudience = Audience,
                        IssuerSigningKey = new
                        SymmetricSecurityKey(Encoding.UTF8.GetBytes(TokenKey))

                    };
                }
                );
            #endregion

            services.AddControllers();

            #region SWAGGER INTEGRATION
            services.AddSwaggerGen(s =>
            {
                s.SwaggerDoc("v1", new OpenApiInfo { 
                    Version = "v1",
                    Title = "First Web Api",
                    Description = "First web api project for learning",
                    TermsOfService = new Uri("https://example.com/licence"),
                    Contact = new OpenApiContact
                    {
                        Name = "Skill up",
                        Email = "contact@skillup.com",
                        Url = new Uri("https://skillup.mk")
                    },
                    License = new OpenApiLicense
                    {
                        Name = "Skill up",
                        Url = new Uri("https://skillup.mk")
                    }
                });

                s.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme { 
                    Name = "SkillUp",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Description = "JWT Authorization header using the Bearer..."
                });

                s.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new string[] { }
                    }
                });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                s.IncludeXmlComments(xmlPath, true);
            });
            #endregion

            #region AUTOMAPPER
            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new FirstWebApiApp.Utils.EmployeeMapper());
            });
            IMapper mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);


            #endregion

            #region IDENTITY CLASS
            services.AddSingleton<IEmployeeRepository, EmployeeRepository>();
            services.AddSingleton<IEmployeeService, EmployeeService>();

            services.AddDbContext<TelecomContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DB_Connect_Dev")));

            services.AddIdentity<UserNew, IdentityRole>(options => options.SignIn.RequireConfirmedAccount = false)
                .AddEntityFrameworkStores<TelecomContext>();
            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            loggerFactory.AddFile("logs/logs{Date}.txt");

            // Use middleware to serve Swagger
            app.UseSwagger();

            app.UseSwaggerUI(s => 
            {
                s.SwaggerEndpoint("/swagger/v1/swagger.json", "First WebApi");
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
